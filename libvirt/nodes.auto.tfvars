infra-nodes = [
      {
        "nodename"  = "lb",
        "vcpu" = "1",
        "mem_in_gb" = "1",
        "sysdisk_in_gb" = 10,
        "datadisk_in_gb" = 100,
        "net_type" = "static",
        //"mac" = "52:54:00:b2:2f:80", //mac value is optional
        "nodeip_with_mask" = "192.168.202.130/24",
        "nodeip_gateway" = "192.168.202.1"
      },
      {
        "nodename"  = "master1",
        "vcpu" = "2",
        "mem_in_gb" = "5",
        "sysdisk_in_gb" = 10,
        "datadisk_in_gb" = 0,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:81",
        "nodeip_with_mask" = "192.168.202.131/24",
        "nodeip_gateway" = "192.168.202.1"
     },
/*
     {
        "nodename"  = "master2",
        "vcpu" = "2",
        "mem_in_gb" = "5",
        "sysdisk_in_gb" = 10,
        "datadisk_in_gb" = 0,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:82",
        "nodeip_with_mask" = "192.168.202.132/24",
        "nodeip_gateway" = "192.168.202.1"
      },
      {
        "nodename"  = "master3",
        "vcpu" = "2",
        "mem_in_gb" = "5",
        "sysdisk_in_gb" = 10,
        "datadisk_in_gb" = 0,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:83",
        "nodeip_with_mask" = "192.168.202.133/24",
        "nodeip_gateway" = "192.168.202.1"
     },
*/
      {
        "nodename"  = "worker1",
        "vcpu" = "2",
        "mem_in_gb" = "6",
        "sysdisk_in_gb" = 30,
        "datadisk_in_gb" = 50,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:84",
        "nodeip_with_mask" = "192.168.202.134/24",
        "nodeip_gateway" = "192.168.202.1"
     },
     {
        "nodename"  = "worker2",
        "vcpu" = "2",
        "mem_in_gb" = "6",
        "sysdisk_in_gb" = 30,
        "datadisk_in_gb" = 50,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:85",
        "nodeip_with_mask" = "192.168.202.135/24",
        "nodeip_gateway" = "192.168.202.1"
     },
     {
        "nodename"  = "worker3",
        "vcpu" = "2",
        "mem_in_gb" = "6",
        "sysdisk_in_gb" = 30,
        "datadisk_in_gb" = 50,
        "net_type" = "static",
        "mac" = "52:54:00:b2:2f:86",
        "nodeip_with_mask" = "192.168.202.136/24",
        "nodeip_gateway" = "192.168.202.1"
     },
]

