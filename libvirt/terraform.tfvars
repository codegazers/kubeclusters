infra-lab = "labs"
infra-os-base-url = "/virtualization/base_images/focal-server-cloudimg-amd64.img"

# Common Network Configuration
infra-network-name = "labs"
infra-network-domain = "labs.local"
infra-network-nameserver = "8.8.8.8"
infra-network-interface = "ens3"
#infra-network-subnet = ["192.168.202.0/23"] # Only for non-ip-static environments and should not exist in your host.
infra-network-bridge = true #
infra-default-username = "vmuser"
infra-default-password = "vmuser"


kvm_pool = "labs"
kvm_bridge_interface = "br0"


# Configuration for each node
   # - We can avoid using specific mac address removing "mac" key.
   # - Default net_type is dhcp
   #         "net_type" = "static",
   #         "nodeip_with_mask" = "192.168.202.10/24"
