# kubeclusters

### Usage:

1 - Clone this repo
```
git clone https://gitlab.com/codegazers/kubeclusters.git
``` 

2 - Configure the appropriate environment in:

- For kvm-libvirt - [./libvirt/nodes.auto.tfvars](./libvirt/nodes.auto.tfvars)

3 - Configure your Kubernetes environment in [./ansible/vars/variables.yaml](/ansible/vars/variables.yaml)
        - Choose Kubernetes release
        - Choose the appropriate Container Runtime to use:
                - Docker (whit its release version)
        - Containerd

4 - Launch the environment using one of the following macros:

    - make create

5 - Other options can be used for easy redeployment such as:

    - make destroy 

    - make recreate


