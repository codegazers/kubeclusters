create:
	cd libvirt && terraform init && terraform apply -auto-approve && cd -
	sleep 120
	cd ansible && ansible-playbook deploy_all.yaml || true && cd -

destroy:
	cd libvirt && terraform destroy -auto-approve && cd -


recreate:
	make destroy create
