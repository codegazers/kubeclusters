#!/bin/bash

[ "$1" != "Iknow" ] && echo "This script should only be execute on either master node or worker nodes. Never execute it on your PC. Use 'Iknow'  as argument to install." && exit 1  

curl -s https://falco.org/repo/falcosecurity-3672BA8F.asc | sudo apt-key add -

echo "deb https://download.falco.org/packages/deb stable main" | sudo tee -a /etc/apt/sources.list.d/falcosecurity.list

sudo apt-get update -qq

sudo apt-get install -qq linux-headers-$(uname -r)

sudo apt-get install -qq falco

