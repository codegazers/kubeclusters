
[https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner](https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner)


On NFS server node:
```
# pvcreate /dev/vdb
# vgcreate vgdata /dev/vdb
# lvcreate --name lvdata -l +100%FREE vgdata
# mkfs.xfs /dev/mapper/vgdata-lvdata 
# mkdir /data
# echo "/dev/mapper/vgdata-lvdata /data xfs defaults 0 0" >>/etc/fstab
# mount -a

# apt-get install nfs-kernel-server -qq
# echo "/data \*(rw,sync,no_subtree_check)" >>/etc/exports

# exportfs -a
# showmount -e localhost
```


On Kubernetes:
```
$ helm repo add nfs-subdir-external-provisioner https://kubernetes-sigs.github.io/nfs-subdir-external-provisioner/

$ helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
    --set nfs.server=192.168.202.130 \
    --set nfs.path=/data
```
