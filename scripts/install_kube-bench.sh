#!/bin/bash

wget https://github.com/aquasecurity/kube-bench/releases/download/v0.6.5/kube-bench_0.6.5_linux_amd64.tar.gz

tar -xvf kube-bench_0.6.5_linux_amd64.tar.gz

rm -rf kube-bench_0.6.5_linux_amd64.tar.gz