#!/bin/bash -x

declare -A NAMESPACES

NAMESPACES['red']='env=dev'
NAMESPACES['blue']='env=dev'
NAMESPACES['green']='env=prod'


MAINGROUP="codegazers"

CURRENT_DIR=../current

KUBECONFIG=${CURRENT_DIR}/kubeconfig

KUBECTL_AS_ADMIN="kubectl --kubeconfig=${KUBECONFIG} --context kubernetes-admin@kubernetes"


## NAMESPACES_CREATION
mkdir ${CURRENT_DIR}/namespaces 2>/dev/null

for namespace in "${!NAMESPACES[@]}"
do
    ${KUBECTL_AS_ADMIN} create namespace ${namespace}
    ${KUBECTL_AS_ADMIN} label namespace ${namespace} ${NAMESPACES[$namespace]} --overwrite
    ${KUBECTL_AS_ADMIN} label namespace ${namespace} maingroup=${MAINGROUP} --overwrite

done


