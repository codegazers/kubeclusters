kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/baremetal/deploy.yaml


kubectl patch svc -n ingress-nginx ingress-nginx-controller --patch \
'{"spec": { "type": "NodePort", "ports": [ { "nodePort": 30080, "port": 80, "protocol": "TCP", "targetPort": 80 } ] } }'

kubectl patch svc -n ingress-nginx ingress-nginx-controller --patch \
'{"spec": { "type": "NodePort", "ports": [ { "nodePort": 30443, "port": 443, "protocol": "TCP", "targetPort": 443 } ] } }'
