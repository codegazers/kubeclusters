#!/bin/bash -x



declare -A USERS

USERS['kurt']='green'
USERS['jim']='blue'
USERS['chris']='blue'
USERS['bowie']='red'


MAINGROUP="codegazers"

CURRENT_DIR=../current

KUBECONFIG=${CURRENT_DIR}/kubeconfig

KUBECTL_AS_ADMIN="kubectl --kubeconfig=${KUBECONFIG} --context kubernetes-admin@kubernetes"

create_k8s_csr(){
cat <<-EOF >${CURRENT_DIR}/users/${user}.csr.yml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
    name: ${1}
spec:
  groups:
  - system:authenticated
  request: $(cat ${CURRENT_DIR}/users/${1}.csr | base64 | tr -d '\n')
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 31536000  # one year
  usages:
  - client auth
EOF
}




## USERS_CREATION
mkdir ${CURRENT_DIR}/users 2>/dev/null

for user in "${!USERS[@]}"
do
    openssl genrsa -out ${CURRENT_DIR}/users/${user}.key 4096
    openssl req -new \
    -subj="/CN=${user},/O=${MAINGROUP},/O=${USER[$user]}" \
    -key=${CURRENT_DIR}/users/${user}.key \
    -out=${CURRENT_DIR}/users/${user}.csr

    create_k8s_csr ${user}

    ${KUBECTL_AS_ADMIN} apply -f ${CURRENT_DIR}/users/${user}.csr.yml

    ${KUBECTL_AS_ADMIN} certificate approve ${user}

done



