#!/bin/bash

[ "$1" != "Iknow" ] && echo "This script should only be execute on either master node or worker nodes. Never execute it on your PC. Use 'Iknow'  as argument to install." && exit 1  

curl -s https://s3.amazonaws.com/download.draios.com/DRAIOS-GPG-KEY.public | sudo apt-key add -  

sudo curl -s -o /etc/apt/sources.list.d/draios.list https://s3.amazonaws.com/download.draios.com/stable/deb/draios.list  

sudo apt-get update -qq

sudo apt-get install -qq linux-headers-$(uname -r)

sudo apt-get install -qq sysdig
